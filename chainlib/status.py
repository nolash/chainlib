# standard imports
import enum

class Status(enum.Enum):
    PENDING = 0
    SUCCESS = 1
    ERROR = 2
