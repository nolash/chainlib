# standard imports
import enum


class BlockSpec(enum.IntEnum):
    PENDING = -1
    LATEST = 0
